package dev.valex.papichrofl.news.views

import android.content.Context
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import dev.valex.papichrofl.general.Papich.Companion.app

class NewsRecyclerView : RecyclerView {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    fun init() {
        if (adapter == null) {
            setHasFixedSize(true)

            layoutManager = LinearLayoutManager(context)

            val itemDecoration = DividerItemDecoration(context, 0)
            addItemDecoration(itemDecoration)

            val itemAnimator = DefaultItemAnimator()
            setItemAnimator(itemAnimator)
        }
    }
}