package dev.valex.papichrofl.news.adapters

import android.databinding.BindingAdapter
import dev.valex.papichrofl.general.Papich.Companion.app
import dev.valex.papichrofl.news.views.NewsRecyclerView

@BindingAdapter("init")
fun bindItemClicked(recyclerView: NewsRecyclerView, init: Boolean) {
    if (recyclerView.adapter == null) {
        recyclerView.init()
        recyclerView.adapter = app.newsAdapter()
    }
}