package dev.valex.papichrofl.news.models

import android.databinding.BaseObservable
import com.android.databinding.library.baseAdapters.BR
import com.google.firebase.firestore.QueryDocumentSnapshot
import java.text.SimpleDateFormat
import java.util.*

class NewsItem(var id: String = "",
               var title: String = "",
               var html: String = "",
               var date: Date = Date()) : BaseObservable() {

    fun getDateText(): String {
        val format = SimpleDateFormat("d MMMM yyyy", Locale.getDefault())
        return format.format(date)
    }

    fun updateData(item: NewsItem) {
        this.title = item.title
        this.html = item.html
        this.date = item.date
        notifyPropertyChanged(BR._all)
    }

    companion object {
        fun fromDocument(document: QueryDocumentSnapshot): NewsItem {
            val documentId = document.id
            val newsItem = document.toObject(NewsItem::class.java)
            newsItem.id = documentId
            return newsItem
        }
    }
}