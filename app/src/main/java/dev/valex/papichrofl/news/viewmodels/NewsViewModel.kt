package dev.valex.papichrofl.news.viewmodels

import android.databinding.BaseObservable
import android.databinding.ObservableBoolean
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.Query
import dev.valex.papichrofl.general.Papich.Companion.app
import dev.valex.papichrofl.news.models.NewsItem
import javax.inject.Inject

class NewsViewModel @Inject constructor() : BaseObservable() {

    private val newsReference = FirebaseFirestore.getInstance().collection("news")
    private var newsListener: ListenerRegistration? = null

    var emptyViewVisible: ObservableBoolean = ObservableBoolean(true)

    fun refreshItems() {
        app.newsAdapter().clear()
        newsListener?.remove()
        newsListener = newsReference
                .orderBy("date", Query.Direction.DESCENDING)
                .addSnapshotListener { data, exc ->
                    if (data != null) {
                        for (change in data.documentChanges) {
                            val type = change.type
                            val document = change.document
                            try {
                                val newsItem = NewsItem.fromDocument(document)
                                when (type) {
                                    DocumentChange.Type.ADDED -> {
                                        app.newsAdapter().addItem(newsItem)
                                        emptyViewVisible.set(app.newsAdapter().itemCount == 0)
                                    }
                                    DocumentChange.Type.MODIFIED -> {
                                        app.newsAdapter().updateItem(newsItem)
                                    }
                                    DocumentChange.Type.REMOVED -> {
                                        app.newsAdapter().deleteItem(newsItem)
                                        emptyViewVisible.set(app.newsAdapter().itemCount == 0)
                                    }
                                }
                            }
                            catch (ignored: Throwable) {

                            }
                        }
                    }
                }
    }
}