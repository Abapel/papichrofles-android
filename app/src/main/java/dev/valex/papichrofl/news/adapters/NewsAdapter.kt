package dev.valex.papichrofl.news.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import dev.valex.papichrofl.databinding.ItemNewsBinding
import dev.valex.papichrofl.news.models.NewsItem

class NewsAdapter: RecyclerView.Adapter<NewsAdapter.ViewHolder>() {

    private var mItems: ArrayList<NewsItem> = ArrayList()

    fun clear() {
        mItems.clear()
        notifyDataSetChanged()
    }

    fun addItem(item: NewsItem) {
        mItems.add(item)
        notifyItemInserted(mItems.size-1)
    }

    fun updateItem(item: NewsItem) {
        val position = mItems.indexOfFirst { it.id == item.id }
        if (position >= 0) {
            mItems[position].updateData(item)
        }
    }

    fun deleteItem(item: NewsItem) {
        val position = mItems.indexOfFirst { it.id == item.id }
        if (position >= 0) {
            mItems.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    override fun getItemCount(): Int = mItems.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemNewsBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.item = mItems[position]
    }

    class ViewHolder(val binding: ItemNewsBinding): RecyclerView.ViewHolder(binding.root)
}