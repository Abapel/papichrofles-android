package dev.valex.papichrofl.news.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dev.valex.papichrofl.R
import dev.valex.papichrofl.databinding.FragmentNewsBinding
import dev.valex.papichrofl.general.Papich.Companion.app
import dev.valex.papichrofl.general.base.BaseFragment

class NewsFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = DataBindingUtil.inflate<FragmentNewsBinding>(inflater, R.layout.fragment_news, container, false)
        binding.model = app.newsViewModel()
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        app.newsViewModel().refreshItems()
    }

    override fun onPause() {
        super.onPause()
    }
}