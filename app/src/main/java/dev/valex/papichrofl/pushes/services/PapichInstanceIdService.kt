package dev.valex.papichrofl.pushes.services

import com.google.firebase.iid.FirebaseInstanceIdService
import com.google.firebase.messaging.FirebaseMessaging

class PapichInstanceIdService : FirebaseInstanceIdService() {

    override fun onTokenRefresh() {
        super.onTokenRefresh()
        FirebaseMessaging.getInstance().subscribeToTopic(PUSH_TOPIC)
    }
}