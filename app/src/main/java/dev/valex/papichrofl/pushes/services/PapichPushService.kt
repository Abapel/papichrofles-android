package dev.valex.papichrofl.pushes.services

import android.app.PendingIntent
import android.content.Intent
import android.graphics.*
import android.media.RingtoneManager
import android.net.Uri
import android.os.Handler
import android.support.v4.app.NotificationCompat
import android.widget.RemoteViews
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import dev.valex.papichrofl.R
import dev.valex.papichrofl.general.Papich.Companion.app
import dev.valex.papichrofl.rofles.activities.RoflesActivity
import android.os.Looper
import dev.valex.papichrofl.general.utils.log
import dev.valex.papichrofl.navigation.activities.NavigationActivity
import kotlin.math.log
import kotlin.math.roundToInt

const val PUSH_TOPIC = "papich"
const val PUSH_TOPIC_TEST = "test"

class PapichPushService : FirebaseMessagingService() {

    private val CHANNEL_ID = "papich"
    private val KEY_TITLE = "title"
    private val KEY_SUBTITLE = "subtitle"
    private val KEY_IMAGE = "image"

    private fun RemoteMessage.hasRequiredFields(): Boolean {
        return data != null && data.containsKey(KEY_TITLE) && data.containsKey(KEY_SUBTITLE)
    }

    private fun RemoteMessage.hasImageUrl(): Boolean {
        return data != null && data[KEY_IMAGE]?.isNotEmpty() == true
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)

        if (app.settings().isNotificationsEnabled() && remoteMessage?.hasRequiredFields() == true) {
            showNotification(remoteMessage)
        }
    }

    private fun showNotification(remoteMessage: RemoteMessage) {
        val title = remoteMessage.data[KEY_TITLE]
        val titleBitmap = getTextAsBitmap(title!!)

        val subtitle = remoteMessage.data[KEY_SUBTITLE]
        val subtitleBitmap = getTextAsBitmap(subtitle!!)

        if (remoteMessage.hasImageUrl()) {
            val url = remoteMessage.data[KEY_IMAGE]
            val uri = Uri.parse(url)
            val imageSize = app.resources().getDimensionPixelSize(R.dimen.notification_image_size)

            val handler = Handler(Looper.getMainLooper())
            handler.post({
                Glide
                    .with(this)
                    .asBitmap()
                    .load(uri)
                    .into(object : SimpleTarget<Bitmap>(imageSize, imageSize) {
                        override fun onResourceReady(bitmap: Bitmap, transition: Transition<in Bitmap>?) {
                            showNotificationWithData(titleBitmap, subtitleBitmap, bitmap)
                        }
                    })
            })
        } else {
            showNotificationWithData(titleBitmap, subtitleBitmap)
        }
    }

    private fun getTextAsBitmap(text: String): Bitmap {
        val paint = Paint()
        paint.textSize = app.resources().getDimensionPixelSize(R.dimen.notification_text_size).toFloat()
        paint.typeface = Typeface.createFromAsset(app.assets(), "fonts/MarkerFelt.ttf")
        paint.color = Color.WHITE
        paint.textAlign = Paint.Align.LEFT

        val baseline = -paint.ascent()
        val width = paint.measureText(text).roundToInt()
        val height = (baseline + paint.descent()).roundToInt()

        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        canvas.drawText(text, 0F, baseline, paint)
        return bitmap
    }

    private fun showNotificationWithData(title: Bitmap,
                                         subtitle: Bitmap,
                                         bitmap: Bitmap = BitmapFactory.decodeResource(app.resources(), R.mipmap.ic_launcher)) {

        val intent = Intent(this, NavigationActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

        val contentView = RemoteViews(packageName, R.layout.item_notification)
        contentView.setImageViewBitmap(R.id.title, title)
        contentView.setImageViewBitmap(R.id.subtitle, subtitle)
        contentView.setImageViewBitmap(R.id.image, bitmap)

        val notificaion = NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContent(contentView)
                .setContentIntent(pendingIntent)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .build()

        app.notificationManager().notify(1, notificaion)
    }
}