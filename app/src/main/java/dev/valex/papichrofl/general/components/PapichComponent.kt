package dev.valex.papichrofl.general.components

import android.app.DownloadManager
import android.app.NotificationManager
import android.content.Context
import android.content.res.AssetManager
import android.content.res.Resources
import android.view.inputmethod.InputMethodManager
import dagger.Component
import dev.valex.papichrofl.general.Papich
import dev.valex.papichrofl.general.modules.PapichModule
import dev.valex.papichrofl.general.modules.ViewModelsModule
import dev.valex.papichrofl.general.annotations.PapichApp
import dev.valex.papichrofl.general.modules.InjectorsModule
import dev.valex.papichrofl.general.settings.Settings
import dev.valex.papichrofl.navigation.viewmodels.NavigationViewModel
import dev.valex.papichrofl.news.adapters.NewsAdapter
import dev.valex.papichrofl.news.viewmodels.NewsViewModel
import dev.valex.papichrofl.reportissue.viewmodels.ReportIssueViewModel
import dev.valex.papichrofl.rofles.adapters.RoflesAdapter
import dev.valex.papichrofl.rofles.network.Network
import dev.valex.papichrofl.rofles.viewmodels.RoflesViewModel
import dev.valex.papichrofl.settings.viewmodels.SettingsViewModel

@Component(modules = [PapichModule::class,
                      ViewModelsModule::class,
                      InjectorsModule::class])
@PapichApp
interface PapichComponent {
    fun inject(app: Papich)

    fun context(): Context
    fun resources(): Resources
    fun assets(): AssetManager
    fun keyboard(): InputMethodManager
    fun settings(): Settings
    fun roflesAdapter(): RoflesAdapter
    fun newsAdapter(): NewsAdapter
    fun downloadManager(): DownloadManager
    fun roflesViewModel(): RoflesViewModel
    fun newsViewModel(): NewsViewModel
    fun reportIssueViewModel(): ReportIssueViewModel
    fun settingsViewModel(): SettingsViewModel
    fun network(): Network
    fun notificationManager(): NotificationManager
    fun navigationViewModel(): NavigationViewModel
}