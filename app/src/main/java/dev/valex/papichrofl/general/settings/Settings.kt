package dev.valex.papichrofl.general.settings

import android.content.Context
import android.preference.PreferenceManager

class Settings(context: Context) {

    private val keyNotificationsEnabled = "notifications_enabled"
    private val keySortByLikes = "sort_by_likes"

    private val prefs = PreferenceManager.getDefaultSharedPreferences(context)

    fun setNotificationsEnabled(enabled: Boolean) = prefs.edit().putBoolean(keyNotificationsEnabled, enabled).apply()

    fun isNotificationsEnabled() = prefs.getBoolean(keyNotificationsEnabled, true)

    fun setSortByLikes(value: Boolean) = prefs.edit().putBoolean(keySortByLikes, value).apply()

    fun isSortByLikes() = prefs.getBoolean(keySortByLikes, true)
}