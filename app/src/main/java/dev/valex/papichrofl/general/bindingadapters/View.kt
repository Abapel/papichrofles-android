package dev.valex.papichrofl.general.bindingadapters

import android.databinding.BindingAdapter
import android.view.View
import android.widget.FrameLayout
import dev.valex.papichrofl.general.base.BaseActivity
import dev.valex.papichrofl.general.base.BaseFragment

@BindingAdapter("visibility")
fun bindVisibility(view: View, isVisible: Boolean) {
    view.visibility = if (isVisible) View.VISIBLE else View.GONE
}

@BindingAdapter("visibleFragment")
fun bindVisibleFragment(frameLayout: FrameLayout, fragment: BaseFragment?) {
    if (frameLayout.context is BaseActivity && fragment != null) {
        val activity = frameLayout.context as BaseActivity
        val transaction = activity.fragmentManager.beginTransaction()
        transaction.replace(frameLayout.id, fragment)
        transaction.commit()
    }
}