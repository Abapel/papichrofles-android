package dev.valex.papichrofl.general.modules

import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dev.valex.papichrofl.general.annotations.ActivityScope
import dev.valex.papichrofl.general.annotations.PapichApp
import dev.valex.papichrofl.rofles.viewmodels.RoflesViewModel

@Module
class ViewModelsModule {

    //@Provides
    //@ActivityScope
    //fun provideRoflesViewModel(): RoflesViewModel = RoflesViewModel()
}