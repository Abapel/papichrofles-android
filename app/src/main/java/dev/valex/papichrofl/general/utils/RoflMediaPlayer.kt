package dev.valex.papichrofl.general.utils

import android.media.MediaPlayer
import dev.valex.papichrofl.rofles.models.Rofl

class RoflMediaPlayer : MediaPlayer() {
    var rofl: Rofl? = null

    override fun stop() {
        super.stop()
        rofl?.playing?.set(false)
    }

    fun playRofl(rofl: Rofl) {
        this.rofl = rofl
        if (isPlaying) {
            stop()
            reset()
        }
        setDataSource(rofl.getFile().path)
        prepare()
        start()
    }
}