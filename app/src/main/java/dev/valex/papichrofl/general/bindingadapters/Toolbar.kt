package dev.valex.papichrofl.general.bindingadapters

import android.databinding.BindingAdapter
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.view.Gravity
import dev.valex.papichrofl.general.utils.findParentDrawerLayout
import dev.valex.papichrofl.general.utils.getActivity

@BindingAdapter(value = ["toolbar", "backVisible"], requireAll = false)
fun bindSupportToolbar(toolbar: Toolbar, bind: Boolean, backVisible: Boolean = false) {
    if (bind) {
        val activity = getActivity(toolbar.context)
        activity?.setSupportActionBar(toolbar)
        activity?.supportActionBar?.setDisplayHomeAsUpEnabled(backVisible)
        activity?.supportActionBar?.setDisplayShowHomeEnabled(backVisible)
    }
}

@BindingAdapter("syncWithDrawer")
fun bindSyncWithDrawer(toolbar: Toolbar, sync: Boolean) {
    val activity = getActivity(toolbar.context)
    val drawerLayout = findParentDrawerLayout(toolbar)
    if (sync && activity != null && drawerLayout != null) {

        bindSupportToolbar(toolbar, true)

        val drawerToggle = ActionBarDrawerToggle(activity, drawerLayout, toolbar, android.R.string.yes, android.R.string.no)
        drawerToggle.isDrawerIndicatorEnabled = true
        drawerLayout.addDrawerListener(drawerToggle)
        drawerToggle.syncState()
    }
}

@BindingAdapter("closeDrawerSignal")
fun bindCloseDrawerSignal(drawerLayout: DrawerLayout, anyValue: Boolean) {
    drawerLayout.closeDrawer(Gravity.START)
}