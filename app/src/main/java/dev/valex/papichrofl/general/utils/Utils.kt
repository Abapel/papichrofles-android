package dev.valex.papichrofl.general.utils

import android.app.DownloadManager
import android.content.Context
import android.content.ContextWrapper
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.text.Html
import android.text.Spanned
import android.view.View
import android.view.ViewParent
import dev.valex.papichrofl.general.Papich.Companion.app

val TAG = "AppTag"

fun log(any: Any?) = System.out.println("$TAG: $any")

fun getActivity(context: Context?): AppCompatActivity? {
    if (context is AppCompatActivity) {
        return context
    }
    else if (context is ContextWrapper) {
        return getActivity(context.baseContext)
    }
    return null
}

fun findParentDrawerLayout(view: View): DrawerLayout? {
    var parent: ViewParent? = view.parent
    if (parent is DrawerLayout) {
        return parent
    }
    while (parent != null) {
        parent = parent.parent
        if (parent is DrawerLayout) {
            return parent
        }
    }
    return null
}

fun DownloadManager.download(name: String, uri: Uri) {
    app.downloadManager().enqueue(DownloadManager.Request(uri)
            .setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
            .setAllowedOverRoaming(true)
            .setTitle(name)
            .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "$name.mp3"))
}

fun fromHtml(html: String): Spanned {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
    } else {
        return Html.fromHtml(html)
    }
}

