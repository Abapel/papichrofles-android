package dev.valex.papichrofl.general.extentions

import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import dev.valex.papichrofl.general.Papich.Companion.analytics
import dev.valex.papichrofl.general.Papich.Companion.getUserId
import dev.valex.papichrofl.rofles.models.Rofl

private const val EVENT_ROFL_PLAY = "rofl_play"
private const val EVENT_ROFL_DOWNLOAD = "rofl_download"
private const val EVENT_ROFL_DOWNLOAD_FILE = "rofl_download_file"
private const val EVENT_ROFL_SHARE = "rofl_share"
private const val EVENT_ROFL_LIKE = "rofl_like"
private const val EVENT_ROFL_DISLIKE = "rofl_dislike"
private const val EVENT_SHARE_APP = "share_app"
private const val EVENT_RATE_APP = "rate_app"
private const val EVENT_OPEN_SETTINGS = "open_settings"
private const val EVENT_OPEN_NEWS = "open_news"
private const val EVENT_NOTIFICATIONS_ENABLED = "notifications_enabled"
private const val EVENT_NOTIFICATIONS_DISABLED = "notifications_disabled"
private const val EVENT_SORT_BY_LIKES = "sort_by_likes"
private const val EVENT_SORT_BY_DATE = "sort_by_date"

private const val KEY_ID = "id"
private const val KEY_NAME = "name"

fun Rofl.getAnalyticsParams(): Bundle {
    val params = Bundle()
    params.putString(KEY_ID, id)
    params.putString(KEY_NAME, name)
    return params
}

fun getUserIdBundle(): Bundle {
    val params = Bundle()
    params.putString(KEY_ID, getUserId())
    return params
}

fun FirebaseAnalytics.onNotificationsEnabled() {
    analytics.logEvent(EVENT_NOTIFICATIONS_ENABLED, getUserIdBundle())
}

fun FirebaseAnalytics.onNotificationsDisabled() {
    analytics.logEvent(EVENT_NOTIFICATIONS_DISABLED, getUserIdBundle())
}

fun FirebaseAnalytics.onSortByLikes() {
    analytics.logEvent(EVENT_SORT_BY_LIKES, getUserIdBundle())
}

fun FirebaseAnalytics.onSortByDate() {
    analytics.logEvent(EVENT_SORT_BY_DATE, getUserIdBundle())
}

fun FirebaseAnalytics.onOpenSettings() {
    analytics.logEvent(EVENT_OPEN_SETTINGS, getUserIdBundle())
}

fun FirebaseAnalytics.onOpenNews() {
    analytics.logEvent(EVENT_OPEN_NEWS, getUserIdBundle())
}

fun FirebaseAnalytics.onShareApp() {
    analytics.logEvent(EVENT_SHARE_APP, getUserIdBundle())
}

fun FirebaseAnalytics.onRateApp() {
    analytics.logEvent(EVENT_RATE_APP, getUserIdBundle())
}

fun FirebaseAnalytics.onRoflPlay(rofl: Rofl) {
    analytics.logEvent(EVENT_ROFL_PLAY, rofl.getAnalyticsParams())
}

fun FirebaseAnalytics.onRoflDownload(rofl: Rofl) {
    analytics.logEvent(EVENT_ROFL_DOWNLOAD, rofl.getAnalyticsParams())
}

fun FirebaseAnalytics.onRoflDownloadFile(rofl: Rofl) {
    analytics.logEvent(EVENT_ROFL_DOWNLOAD_FILE, rofl.getAnalyticsParams())
}

fun FirebaseAnalytics.onRoflShare(rofl: Rofl) {
    analytics.logEvent(EVENT_ROFL_SHARE, rofl.getAnalyticsParams())
}

fun FirebaseAnalytics.onRoflLike(rofl: Rofl) {
    analytics.logEvent(EVENT_ROFL_LIKE, rofl.getAnalyticsParams())
}

fun FirebaseAnalytics.onRoflDislike(rofl: Rofl) {
    analytics.logEvent(EVENT_ROFL_DISLIKE, rofl.getAnalyticsParams())
}