package dev.valex.papichrofl.general

import android.app.Activity
import android.app.Application
import android.support.multidex.MultiDexApplication
import android.support.v7.app.AppCompatDelegate
import android.support.v7.app.AppCompatDelegate.*
import com.crashlytics.android.Crashlytics
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dev.valex.papichrofl.general.components.DaggerPapichComponent
import dev.valex.papichrofl.general.components.PapichComponent
import dev.valex.papichrofl.general.modules.PapichModule
import dev.valex.papichrofl.general.modules.ViewModelsModule
import dev.valex.papichrofl.general.utils.log
import javax.inject.Inject
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import dev.valex.papichrofl.BuildConfig
import dev.valex.papichrofl.R


class Papich : MultiDexApplication(), HasActivityInjector {

    @Inject lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingActivityInjector

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setDefaultNightMode(MODE_NIGHT_YES)

        app = DaggerPapichComponent.builder()
                .papichModule(PapichModule(this))
                .viewModelsModule(ViewModelsModule())
                .build()
        app.inject(this)

        analytics = FirebaseAnalytics.getInstance(this)

        config = FirebaseRemoteConfig.getInstance()
        val configSettings = FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build()
        config.setConfigSettings(configSettings)
        config.setDefaults(R.xml.remote_config_defaults)

        user = FirebaseAuth.getInstance().currentUser
        if (user == null) {
            FirebaseAuth.getInstance().signInAnonymously().addOnCompleteListener {task ->
                user = FirebaseAuth.getInstance().currentUser
            }
        }
    }

    companion object {
        lateinit var app: PapichComponent
        lateinit var analytics: FirebaseAnalytics
        lateinit var config: FirebaseRemoteConfig
        var user: FirebaseUser? = null

        fun getUserId(): String {
            return FirebaseAuth.getInstance().currentUser?.uid ?: ""
        }
    }
}