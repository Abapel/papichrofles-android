package dev.valex.papichrofl.general.bindingadapters

import android.databinding.BindingAdapter
import android.graphics.Typeface
import android.widget.EditText
import android.widget.TextView
import dev.valex.papichrofl.general.Papich.Companion.app
import android.support.design.widget.TextInputLayout
import android.text.method.LinkMovementMethod
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import dev.valex.papichrofl.general.utils.fromHtml

@BindingAdapter("font")
fun bindFont(button: Button, font: String) {
    button.typeface = Typeface.createFromAsset(app.assets(), font)
}

@BindingAdapter("font")
fun bindFont(textView: TextView, font: String) {
    textView.typeface = Typeface.createFromAsset(app.assets(), font)
}

@BindingAdapter("font")
fun bindFont(textView: EditText, font: String) {
    textView.typeface = Typeface.createFromAsset(app.assets(), font)
}

@BindingAdapter("font")
fun bindFontTextInputLayout(textInputLayout: TextInputLayout, font: String) {
    textInputLayout.setTypeface(Typeface.createFromAsset(app.assets(), font))
}

@BindingAdapter("resetText")
fun bindFont(textView: EditText, resetText: Boolean) {
    if (resetText) {
        textView.setText("")
    }
}

@BindingAdapter("selection")
fun bindFont(textView: EditText, position: Int) {
    if (textView.selectionStart == 0 && position > 0) {
        textView.setSelection(position)
    }
}

@BindingAdapter("keyboardVisible")
fun bindKeyboardVisible(textView: EditText, visible: Boolean) {
    if (visible) {
        textView.requestFocus()
        app.keyboard().show()
    }
    else {
        app.keyboard().hide(textView)
    }
}

@BindingAdapter("html")
fun bindHtml(textView: TextView, html: String) {
    textView.text = fromHtml(html)
    textView.movementMethod = LinkMovementMethod.getInstance()
}

fun InputMethodManager.show() {
    toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
}

fun InputMethodManager.hide(fromView: TextView) {
    hideSoftInputFromWindow(fromView.windowToken, 0)
}