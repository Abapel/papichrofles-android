package dev.valex.papichrofl.general.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dev.valex.papichrofl.general.annotations.ActivityScope
import dev.valex.papichrofl.rofles.activities.RoflesActivity

@Module(includes = [AndroidSupportInjectionModule::class])
interface InjectorsModule {

    /*
    @ActivityScope
    @ContributesAndroidInjector(modules = [ViewModelsModule::class])
    fun roflesActivityInjector(): RoflesActivity
    */
}