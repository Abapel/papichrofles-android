package dev.valex.papichrofl.general.modules

import android.app.DownloadManager
import android.app.NotificationManager
import android.content.Context
import android.content.res.AssetManager
import android.content.res.Resources
import android.view.inputmethod.InputMethodManager
import com.google.firebase.messaging.FirebaseMessagingService
import dagger.Module
import dagger.Provides
import dev.valex.papichrofl.BuildConfig
import dev.valex.papichrofl.general.annotations.PapichApp
import dev.valex.papichrofl.general.settings.Settings
import dev.valex.papichrofl.navigation.viewmodels.NavigationViewModel
import dev.valex.papichrofl.news.adapters.NewsAdapter
import dev.valex.papichrofl.news.viewmodels.NewsViewModel
import dev.valex.papichrofl.reportissue.viewmodels.ReportIssueViewModel
import dev.valex.papichrofl.rofles.adapters.RoflesAdapter
import dev.valex.papichrofl.rofles.network.Network
import dev.valex.papichrofl.rofles.viewmodels.RoflesViewModel
import dev.valex.papichrofl.settings.viewmodels.SettingsViewModel
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class PapichModule(var mContext: Context) {

    @Provides
    @PapichApp
    fun provideContext(): Context = mContext

    @Provides
    @PapichApp
    fun provideResources(): Resources = mContext.resources

    @Provides
    @PapichApp
    fun provideAssets(): AssetManager = mContext.assets

    @Provides
    @PapichApp
    fun provideKeyboard(): InputMethodManager = mContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

    @Provides
    @PapichApp
    fun provideSettings(): Settings = Settings(mContext)

    @Provides
    @PapichApp
    fun provideRoflesAdapter(): RoflesAdapter = RoflesAdapter()

    @Provides
    @PapichApp
    fun provideNewsAdapter(): NewsAdapter = NewsAdapter()

    @Provides
    @PapichApp
    fun provideNotificationManager(): NotificationManager = mContext.getSystemService(FirebaseMessagingService.NOTIFICATION_SERVICE) as NotificationManager

    @Provides
    @PapichApp
    fun provideDownloadmanager(): DownloadManager = mContext.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager

    @Provides
    @PapichApp
    fun provideNavigationViewModel(): NavigationViewModel = NavigationViewModel()

    @Provides
    @PapichApp
    fun provideRoflesViewModel(): RoflesViewModel = RoflesViewModel()

    @Provides
    @PapichApp
    fun provideNewsViewModel(): NewsViewModel = NewsViewModel()

    @Provides
    @PapichApp
    fun provideReportIssueViewModel(): ReportIssueViewModel = ReportIssueViewModel()

    @Provides
    @PapichApp
    fun provideSettingsViewModel(): SettingsViewModel = SettingsViewModel()

    @Provides
    @PapichApp
    fun provideNetwork(): Network = Retrofit.Builder()
            .client(Network.getHttpClient())
            .baseUrl(BuildConfig.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(Network::class.java)
}