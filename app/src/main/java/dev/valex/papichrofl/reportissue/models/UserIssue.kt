package dev.valex.papichrofl.reportissue.models

import android.content.Context
import android.os.Build
import com.google.android.gms.common.GoogleApiAvailability
import dev.valex.papichrofl.general.Papich.Companion.app
import android.telephony.TelephonyManager
import dev.valex.papichrofl.BuildConfig
import java.util.*

class UserIssue(val message: String?) {
    val manufacturer = Build.MANUFACTURER
    val brand = Build.BRAND
    val model = Build.MODEL
    val androidVersion = Build.VERSION.RELEASE
    val androidSdk = Build.VERSION.SDK_INT
    val playServicesStatus = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(app.context())
    val playServicesVersionCode = GoogleApiAvailability.GOOGLE_PLAY_SERVICES_VERSION_CODE
    val networkType = _getNetworkType()
    val date = Date()
    val appVersion = BuildConfig.VERSION_NAME

    private fun _getNetworkType(): String {
        val teleMan = app.context().getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        val networkType = teleMan.networkType
        when (networkType) {
            TelephonyManager.NETWORK_TYPE_1xRTT -> return "1xRTT"
            TelephonyManager.NETWORK_TYPE_CDMA -> return "CDMA"
            TelephonyManager.NETWORK_TYPE_EDGE -> return "EDGE"
            TelephonyManager.NETWORK_TYPE_EHRPD -> return "eHRPD"
            TelephonyManager.NETWORK_TYPE_EVDO_0 -> return "EVDO rev. 0"
            TelephonyManager.NETWORK_TYPE_EVDO_A -> return "EVDO rev. A"
            TelephonyManager.NETWORK_TYPE_EVDO_B -> return "EVDO rev. B"
            TelephonyManager.NETWORK_TYPE_GPRS -> return "GPRS"
            TelephonyManager.NETWORK_TYPE_HSDPA -> return "HSDPA"
            TelephonyManager.NETWORK_TYPE_HSPA -> return "HSPA"
            TelephonyManager.NETWORK_TYPE_HSPAP -> return "HSPA+"
            TelephonyManager.NETWORK_TYPE_HSUPA -> return "HSUPA"
            TelephonyManager.NETWORK_TYPE_IDEN -> return "iDen"
            TelephonyManager.NETWORK_TYPE_LTE -> return "LTE"
            TelephonyManager.NETWORK_TYPE_UMTS -> return "UMTS"
            TelephonyManager.NETWORK_TYPE_UNKNOWN -> return "Unknown"
            else -> return "New type of network"
        }
    }
}