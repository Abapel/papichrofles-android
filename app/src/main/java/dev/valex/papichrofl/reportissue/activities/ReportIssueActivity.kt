package dev.valex.papichrofl.reportissue.activities

import android.databinding.DataBindingUtil
import android.os.Bundle
import dev.valex.papichrofl.R
import dev.valex.papichrofl.databinding.ActivityReportIssueBinding
import dev.valex.papichrofl.general.Papich.Companion.app
import dev.valex.papichrofl.general.base.BaseActivity

class ReportIssueActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityReportIssueBinding>(this, R.layout.activity_report_issue)
        val model = app.reportIssueViewModel()
        model.resetState()
        model.activity = this
        binding.model = model
    }

    override fun onPause() {
        super.onPause()
        app.reportIssueViewModel().onPause()
    }

    override fun onResume() {
        super.onResume()
        app.reportIssueViewModel().onResume()
    }
}