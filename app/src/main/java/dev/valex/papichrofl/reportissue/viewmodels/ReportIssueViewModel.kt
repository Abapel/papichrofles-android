package dev.valex.papichrofl.reportissue.viewmodels

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import com.google.firebase.firestore.FirebaseFirestore
import dev.valex.papichrofl.general.Papich.Companion.app
import dev.valex.papichrofl.general.base.BaseActivity
import dev.valex.papichrofl.general.utils.log
import dev.valex.papichrofl.reportissue.models.UserIssue
import dev.valex.papichrofl.rofles.models.RoflError

class ReportIssueViewModel : BaseObservable() {

    val screenVisible = ObservableBoolean(false)
    var activity: BaseActivity? = null
    val messageMaxLength = 300

    @Bindable
    var messageText: ObservableField<String> = ObservableField("")

    @Bindable
    var loading: ObservableBoolean = ObservableBoolean(false)

    fun onMessageTextChanged(newText: String) {
        messageText.set(newText)
    }

    fun onPause() {
        screenVisible.set(false)
    }

    fun onResume() {
        screenVisible.set(true)
    }

    fun onSendClick() {
        val issue = UserIssue(messageText.get())
        loading.set(true)
        FirebaseFirestore.getInstance()
                .collection("issues")
                .add(issue)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        app.roflesViewModel().setErrorWithCountdown(RoflError("Success"), 5000)
                        messageText.set("")
                        try {
                            activity?.onBackPressed()
                        } catch (ignored: Exception) {
                            log(ignored)
                        }
                    }
                    else {
                        //TODO add show error
                        loading.set(false)
                    }
                }
                .addOnFailureListener { exc ->
                    //TODO add show error on timeout
                    loading.set(false)
                }
    }

    fun resetState() {
        loading.set(false)
    }
}