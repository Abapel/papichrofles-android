package dev.valex.papichrofl.navigation.viewmodels

import android.content.Intent
import android.databinding.BaseObservable
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.net.Uri
import dev.valex.papichrofl.R
import dev.valex.papichrofl.general.Papich.Companion.app
import dev.valex.papichrofl.general.base.BaseFragment
import dev.valex.papichrofl.navigation.fragments.NavigationFragment
import dev.valex.papichrofl.rofles.fragments.RoflesFragment
import dev.valex.papichrofl.settings.fragments.SettingsFragment
import android.support.v4.content.ContextCompat.startActivity
import dev.valex.papichrofl.general.Papich.Companion.analytics
import dev.valex.papichrofl.general.base.BaseActivity
import dev.valex.papichrofl.general.extentions.onOpenNews
import dev.valex.papichrofl.general.extentions.onOpenSettings
import dev.valex.papichrofl.general.extentions.onRateApp
import dev.valex.papichrofl.general.extentions.onShareApp
import dev.valex.papichrofl.news.fragments.NewsFragment

enum class AppScreen(val title: String) {
    ALL_ROFLES(app.context().getString(R.string.all_rofles)),
    FAV_ROFLES(app.context().getString(R.string.favourites)),
    SETTINGS(app.context().getString(R.string.settings)),
    NEWS(app.context().getString(R.string.news))
}

class NavigationViewModel : BaseObservable() {

    var screen = ObservableField<AppScreen>()
    var title = ObservableField<String>("")

    var menuFragment = ObservableField<BaseFragment>(NavigationFragment())
    var contentFragment = ObservableField<BaseFragment>(RoflesFragment())
    var closeDrawerSignal = ObservableBoolean(false)

    var activity: BaseActivity? = null

    init {
        setScreen(AppScreen.ALL_ROFLES)
    }

    fun resume(fromActivity: BaseActivity) {
        activity = fromActivity
    }

    fun pause() {

    }

    fun closeDrawer() {
        closeDrawerSignal.set(!closeDrawerSignal.get())
    }

    fun onAllRoflesClick() {
        closeDrawer()
        if (screen.get() != AppScreen.ALL_ROFLES) {
            app.roflesViewModel().showFavouritesOnly(false)
            setScreen(AppScreen.ALL_ROFLES)
            if (contentFragment.get() !is RoflesFragment) {
                contentFragment.set(RoflesFragment())
            }
        }
    }

    fun onFavouriteRoflesClick() {
        closeDrawer()
        if (screen.get() != AppScreen.FAV_ROFLES) {
            app.roflesViewModel().showFavouritesOnly(true)
            setScreen(AppScreen.FAV_ROFLES)
            if (contentFragment.get() !is RoflesFragment) {
                contentFragment.set(RoflesFragment())
            }
        }
    }

    fun onSettingsClick() {
        closeDrawer()
        if (screen.get() != AppScreen.SETTINGS) {
            app.roflesViewModel().cancelSearch()
            setScreen(AppScreen.SETTINGS)
            contentFragment.set(SettingsFragment())
            analytics.onOpenSettings()
        }
    }

    fun onShareAppClick() {
        closeDrawer()
        val intent = Intent(Intent.ACTION_SEND)
        val text = "${app.context().getString(R.string.google_play_share_text)}: ${app.context().getString(R.string.google_play_url)}"
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_SUBJECT, app.context().getString(R.string.app_name))
        intent.putExtra(Intent.EXTRA_TEXT, text)
        activity?.startActivity(Intent.createChooser(intent, app.context().getString(R.string.share_app)))
        analytics.onShareApp()
    }

    fun onNewsClick() {
        closeDrawer()
        if (screen.get() != AppScreen.NEWS) {
            app.roflesViewModel().cancelSearch()
            setScreen(AppScreen.NEWS)
            contentFragment.set(NewsFragment())
            analytics.onOpenNews()
        }
    }

    fun onRateAppClick() {
        try {
            activity?.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=${app.context().packageName}")))
        } catch (anfe: android.content.ActivityNotFoundException) {
            activity?.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=${app.context().packageName}")))
        }
        analytics.onRateApp()
    }

    private fun setScreen(newScreen: AppScreen) {
        screen.set(newScreen)
        title.set(newScreen.title)
    }
}