package dev.valex.papichrofl.navigation.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dev.valex.papichrofl.R
import dev.valex.papichrofl.databinding.FragmentNavigationBinding
import dev.valex.papichrofl.general.Papich.Companion.app
import dev.valex.papichrofl.general.base.BaseFragment

class NavigationFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = DataBindingUtil.inflate<FragmentNavigationBinding>(inflater, R.layout.fragment_navigation, container, false)
        binding.model = app.navigationViewModel()
        return binding.root
    }
}