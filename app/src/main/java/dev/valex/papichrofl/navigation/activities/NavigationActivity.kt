package dev.valex.papichrofl.navigation.activities

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import dev.valex.papichrofl.R
import dev.valex.papichrofl.databinding.ActivityNavigationBinding
import dev.valex.papichrofl.general.Papich.Companion.app
import dev.valex.papichrofl.general.base.BaseActivity

class NavigationActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityNavigationBinding>(this, R.layout.activity_navigation)
        binding.nav = app.navigationViewModel()
        binding.rofles = app.roflesViewModel()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        app.notificationManager().cancelAll()
    }

    override fun onPause() {
        super.onPause()
        app.navigationViewModel().pause()
    }

    override fun onResume() {
        super.onResume()
        app.navigationViewModel().resume(this)
    }

    companion object {
        val EXTRA_NEWS = "news"
    }
}