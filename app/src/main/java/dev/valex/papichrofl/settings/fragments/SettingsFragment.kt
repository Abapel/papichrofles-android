package dev.valex.papichrofl.settings.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dev.valex.papichrofl.R
import dev.valex.papichrofl.databinding.FragmentSettingsBinding
import dev.valex.papichrofl.general.Papich.Companion.app
import dev.valex.papichrofl.general.base.BaseActivity
import dev.valex.papichrofl.general.base.BaseFragment

class SettingsFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = DataBindingUtil.inflate<FragmentSettingsBinding>(inflater, R.layout.fragment_settings, container, false)
        binding.model = app.settingsViewModel()
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        app.settingsViewModel().resume(activity as BaseActivity)
    }
}