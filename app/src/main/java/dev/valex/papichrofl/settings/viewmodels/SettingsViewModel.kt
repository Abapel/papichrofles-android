package dev.valex.papichrofl.settings.viewmodels

import android.content.Intent
import android.databinding.BaseObservable
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import dev.valex.papichrofl.BuildConfig
import dev.valex.papichrofl.general.Papich
import dev.valex.papichrofl.general.Papich.Companion.analytics
import dev.valex.papichrofl.general.Papich.Companion.app
import dev.valex.papichrofl.general.base.BaseActivity
import dev.valex.papichrofl.general.extentions.onNotificationsDisabled
import dev.valex.papichrofl.general.extentions.onNotificationsEnabled
import dev.valex.papichrofl.general.extentions.onSortByDate
import dev.valex.papichrofl.general.extentions.onSortByLikes
import dev.valex.papichrofl.reportissue.activities.ReportIssueActivity

class SettingsViewModel : BaseObservable() {

    val notificationsEnabled = ObservableBoolean(app.settings().isNotificationsEnabled())
    val sortByLikes = ObservableBoolean(app.settings().isSortByLikes())

    var activity: BaseActivity? = null

    val version: ObservableField<String> = ObservableField()

    init {
        version.set(BuildConfig.VERSION_NAME)
    }

    fun resume(fromActivity: BaseActivity) {
        activity = fromActivity
    }

    fun onNotificationsEnabledClick() {
        val newValue = !notificationsEnabled.get()
        notificationsEnabled.set(newValue)
        app.settings().setNotificationsEnabled(newValue)

        if (newValue) {
            analytics.onNotificationsEnabled()
        }
        else {
            analytics.onNotificationsDisabled()
        }
    }

    fun onSortByLikesClick() {
        sortByLikes.set(true)
        app.settings().setSortByLikes(true)
        analytics.onSortByLikes()
    }

    fun onSortByDateClick() {
        sortByLikes.set(false)
        app.settings().setSortByLikes(false)
        analytics.onSortByDate()
    }

    fun onFeedbackClick() {
        val intent = Intent(Papich.app.context(), ReportIssueActivity::class.java)
        activity?.startActivity(intent)
        //Papich.app.context().startActivity(intent)
    }
}