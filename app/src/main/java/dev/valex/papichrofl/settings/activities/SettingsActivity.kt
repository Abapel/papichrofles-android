package dev.valex.papichrofl.settings.activities

import android.databinding.DataBindingUtil
import android.os.Bundle
import dev.valex.papichrofl.R
import dev.valex.papichrofl.databinding.ActivitySettingsBinding
import dev.valex.papichrofl.general.Papich.Companion.app
import dev.valex.papichrofl.general.base.BaseActivity

class SettingsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivitySettingsBinding>(this, R.layout.activity_settings)
        binding.model = app.settingsViewModel()
    }

    override fun onResume() {
        super.onResume()
        app.settingsViewModel().resume(this)
    }
}