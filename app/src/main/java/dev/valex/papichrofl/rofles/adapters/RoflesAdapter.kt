package dev.valex.papichrofl.rofles.adapters

import android.os.AsyncTask
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import dev.valex.papichrofl.R
import dev.valex.papichrofl.databinding.ItemRoflBinding
import dev.valex.papichrofl.general.Papich.Companion.app
import dev.valex.papichrofl.rofles.enums.RoflAction
import dev.valex.papichrofl.rofles.interfaces.OnRoflActionListener
import dev.valex.papichrofl.rofles.models.Rofl

class RoflesAdapter : RecyclerView.Adapter<RoflesAdapter.ViewHolder>() {

    private var mItems: ArrayList<Rofl> = ArrayList()
    private var mFilteredItems: ArrayList<Rofl> = ArrayList()
    private var mIsSearching: Boolean = false
    private var mIsFavourites: Boolean = false

    var onRoflActionListener: OnRoflActionListener? = null

    fun clear() {
        mItems.clear()
        mFilteredItems.clear()
        mIsSearching = false
        notifyDataSetChanged()
    }

    fun showFavouritesOnly(value: Boolean) {
        if (mIsFavourites != value) {
            mIsFavourites = value
            filterByName("", true)
        }
    }

    fun filterByName(name: String, forceUpdate: Boolean = false) {
        mIsSearching = name.isNotEmpty()
        if (mIsSearching) {
            searchByNameBackground(name)
        }
        else if ((mItems.size != mFilteredItems.size && !mIsFavourites) || forceUpdate) {
            updateListBackground()
        }
    }

    fun searchByNameBackground(name: String) {
        app.roflesViewModel().setBusy(true)
        BackgroundTask(backgroundTask = {
            val filteredItems = mItems.filter {
                if (mIsFavourites)
                    it.getSearchRatio(name) > 60 && it.isLikedByUser()
                else
                    it.getSearchRatio(name) > 60 }
            mFilteredItems.clear()
            mFilteredItems.addAll(filteredItems)
        }, postExecute ={
            notifyDataSetChanged()
            app.roflesViewModel().setBusy(false)
        }).execute()
    }

    fun updateListBackground() {
        app.roflesViewModel().setBusy(true)
        BackgroundTask(backgroundTask = {
            mFilteredItems.clear()
            if (mIsFavourites) {
                mFilteredItems.addAll(mItems.filter { it.isLikedByUser() })
            }
            else {
                mFilteredItems.addAll(mItems)
            }
        }, postExecute = {
            notifyDataSetChanged()
            app.roflesViewModel().setBusy(false)
        }).execute()
    }

    override fun getItemCount(): Int = mFilteredItems.size

    fun setItems(items: List<Rofl>) {
        mItems.clear()
        mItems.addAll(items)
        mFilteredItems.clear()
        mFilteredItems.addAll(items)
        notifyDataSetChanged()
    }

    fun addItem(rofl: Rofl) {
        mItems.add(rofl)
        if (!mIsSearching) {
            mFilteredItems.add(rofl)
            notifyItemInserted(mItems.size-1)
        }
    }

    fun updateItem(rofl: Rofl) {
        val position = mItems.indexOfFirst { it.id == rofl.id }
        if (position >= 0) {
            mItems[position].updatePublicData(rofl)
        }
    }

    fun deleteItem(rofl: Rofl) {
        val position = mItems.indexOfFirst { it.id == rofl.id }
        if (position >= 0) {
            mItems.removeAt(position)
            if (!mIsSearching) {
                mFilteredItems.removeAt(position)
                notifyItemRemoved(position)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemRoflBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mFilteredItems[position]
        holder.binding.rofl = item
        holder.binding.position = position
        holder.binding.listener = onRoflActionListener
        holder.binding.root.setOnCreateContextMenuListener(getContextMenuListener(item))
    }

    fun getContextMenuListener(rofl: Rofl) = View.OnCreateContextMenuListener { menu, v, menuInfo ->
        menu.setHeaderTitle(rofl.name)

        val share = menu.add(0, 0, 0, app.context().getString(R.string.share))
        share.setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener {
            onRoflActionListener?.onActionDone(RoflAction.SHARE, rofl)
            true
        })

        val download = menu.add(0, 1, 0, app.context().getString(R.string.download))
        download.setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener {
            onRoflActionListener?.onActionDone(RoflAction.DOWNLOAD, rofl)
            true
        })
    }

    class ViewHolder(val binding: ItemRoflBinding): RecyclerView.ViewHolder(binding.root)

    class BackgroundTask(val backgroundTask: () -> Unit,
                         val postExecute: () -> Unit) : AsyncTask<Void, Void, Boolean>() {

        override fun doInBackground(vararg params: Void?): Boolean {
            backgroundTask.invoke()
            return true
        }

        override fun onPostExecute(result: Boolean?) {
            super.onPostExecute(result)
            postExecute.invoke()
        }
    }
}