package dev.valex.papichrofl.rofles.views

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout

class RoflProgressBar : RelativeLayout {
    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private var progressView: View? = null

    fun getProgressView(): View {
        if (progressView == null) {
            progressView = View(context)
            progressView!!.layoutParams = layoutParams
            progressView!!.setBackgroundColor(Color.RED)
            addView(progressView)
        }
        return progressView!!
    }

    fun setProgressPercent(progressPercent: Int) {
        getProgressView().animate().translationY(width / 100F * progressPercent).setDuration(100L).start()
    }
}