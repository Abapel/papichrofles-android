package dev.valex.papichrofl.rofles.views

import android.content.Context
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import dev.valex.papichrofl.general.Papich.Companion.app
import dev.valex.papichrofl.rofles.adapters.RoflesAdapter

class RoflRecyclerView : RecyclerView {
    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    fun getRoflesAdapter(): RoflesAdapter {
        if (adapter == null) {
            adapter = app.roflesAdapter()
            setHasFixedSize(true)

            layoutManager = LinearLayoutManager(context)

            val itemDecoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            addItemDecoration(itemDecoration)

            val itemAnimator = DefaultItemAnimator()
            setItemAnimator(itemAnimator)
        }
        return adapter as RoflesAdapter
    }
}