package dev.valex.papichrofl.rofles.models

import android.databinding.*
import android.net.Uri
import android.os.Environment
import android.os.Handler
import com.android.databinding.library.baseAdapters.BR
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.firestore.QueryDocumentSnapshot
import com.google.firebase.storage.FileDownloadTask
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import dev.valex.papichrofl.general.Papich.Companion.app
import dev.valex.papichrofl.general.Papich.Companion.getUserId
import dev.valex.papichrofl.general.utils.log
import me.xdrop.fuzzywuzzy.FuzzySearch
import java.io.File
import kotlin.math.roundToInt
import android.media.RingtoneManager
import android.provider.MediaStore
import android.content.ContentValues
import dev.valex.papichrofl.R
import java.util.*
import kotlin.collections.ArrayList


const val TIMEOUT_SHARE = 5000L
const val TIMEOUT_DOWNLOAD = 15000L

class Rofl(var id: String = "",
           var name: String = "",
           var storage: String = "",
           var date: Date = Date(),
           var likedCount: Int = 0,
           var liked: List<String> = arrayListOf()) : BaseObservable() {

    var preparingForDownload: ObservableBoolean = ObservableBoolean(false)
    var downloadProgressPercent: ObservableInt = ObservableInt(0)
    var playProgressPercent: ObservableInt = ObservableInt(0)
    var playing: ObservableBoolean = ObservableBoolean(false)
    var sharing: ObservableBoolean = ObservableBoolean(false)
    var updatingLike = ObservableBoolean(false)

    fun isDownloading(): Boolean {
        val progress = downloadProgressPercent.get()
        return progress in 1..99 || preparingForDownload.get()
    }

    fun updateSecuredData(name: String, storage: String, date: Date) {
        this.name = name
        this.storage = storage
        this.date = date
        notifyPropertyChanged(BR._all)
    }

    fun updatePublicData(rofl: Rofl) {
        this.name = rofl.name
        this.storage = rofl.storage
        this.date = rofl.date
        this.liked = rofl.liked
        this.likedCount = rofl.likedCount
        notifyPropertyChanged(BR._all)
    }

    fun getFilename(): String {
        val paths = storage.split("/")
        if (paths.isNotEmpty()) {
            return paths[paths.size-1]
        }
        return ""
    }

    fun isLikedByUser(): Boolean {
        val userId = getUserId()
        val like = liked.find { it == userId }
        return like != null
    }

    fun setFakeLiked(isLiked: Boolean) {
        if (isLiked) {
            likedCount++
            val index = liked.indexOf(getUserId())
            if (index == -1) {
                val newLikedList = ArrayList<String>()
                newLikedList.addAll(liked)
                newLikedList.add(getUserId())
                liked = newLikedList
            }
        }
        else {
            likedCount--
            val index = liked.indexOf(getUserId())
            if (index > -1) {
                val newLikedList = ArrayList<String>()
                newLikedList.addAll(liked)
                newLikedList.removeAt(index)
                liked = newLikedList
            }
        }
        notifyPropertyChanged(BR._all)
    }

    fun getStorageReference(): StorageReference = FirebaseStorage.getInstance().getReferenceFromUrl(storage)

    fun getFile(): File = File(getStorageDirectory(), getFilename())

    fun getStorageDirectory(): File? {
        val directory = File(app.context().getExternalFilesDir(Environment.DIRECTORY_MUSIC), "papichRofles")
        if (!directory.mkdirs()) {
            log("Directory not created")
        }
        return directory
    }

    fun isFileExist(): Boolean = getFile().exists()

    fun isPlayingNow(): Boolean = playing.get()

    fun downloadWithTimeout(onSuccess: OnSuccessListener<FileDownloadTask.TaskSnapshot>,
                            onFailure: OnFailureListener,
                            onTimeout: () -> Unit) {

        if (!isDownloading()) {
            var started = false
            var failed = false
            var timedout = false

            Handler().postDelayed({
                if (!started && !failed) {
                    timedout = true
                    preparingForDownload.set(false)
                    onTimeout.invoke()
                }
            }, TIMEOUT_DOWNLOAD)

            preparingForDownload.set(true)
            try {
                val reference = getStorageReference()
                reference.getFile(getFile()).addOnSuccessListener { taskSnaphot ->
                    onSuccess.onSuccess(taskSnaphot)
                    notifyPropertyChanged(BR._all)
                }.addOnFailureListener { exception ->
                    if (!timedout) {
                        failed = true
                        onFailure.onFailure(exception)
                    }
                }.addOnProgressListener { progress ->
                    if (!timedout && progress.bytesTransferred > 0) {
                        started = true
                        val progressValue = 100.0 * progress.bytesTransferred / progress.totalByteCount
                        preparingForDownload.set(progressValue == 0.0)
                        downloadProgressPercent.set(progressValue.roundToInt())
                    }
                }
            }
            catch (exc: Exception) {
                if (!timedout) {
                    preparingForDownload.set(false)
                    failed = true
                    onFailure.onFailure(exc)
                }
            }
        }
    }

    fun getDownloadUrlWithTimeout(onSuccess: OnSuccessListener<Uri>,
                                  onFailure: OnFailureListener,
                                  onTimeout: () -> Unit) {

        sharing.set(true)

        var downloaded = false
        var failed = false
        var timedout = false

        Handler().postDelayed({
            if (!downloaded && !failed) {
                timedout = true
                onTimeout.invoke()
                sharing.set(false)
            }
        }, TIMEOUT_SHARE)

        getStorageReference()
                .downloadUrl
                .addOnSuccessListener { uri ->  
                    if (!timedout) {
                        downloaded = true
                        sharing.set(false)
                        onSuccess.onSuccess(uri)
                    }
                }
                .addOnFailureListener { exception ->  
                    if (!timedout) {
                        failed = true
                        sharing.set(false)
                        onFailure.onFailure(exception)
                    }
                }
    }
    
    fun getSearchRatio(searchText: String): Int = FuzzySearch.partialRatio(searchText.toLowerCase(), name.toLowerCase())

    fun setRingtone() {
        val values = ContentValues()
        val file = getFile()
        values.put(MediaStore.MediaColumns.DATA, file.absolutePath)
        values.put(MediaStore.MediaColumns.TITLE, name)
        values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mp3")
        values.put(MediaStore.MediaColumns.SIZE, file.length())
        values.put(MediaStore.Audio.Media.ARTIST, R.string.app_name)
        values.put(MediaStore.Audio.Media.IS_RINGTONE, true)
        values.put(MediaStore.Audio.Media.IS_NOTIFICATION, false)
        values.put(MediaStore.Audio.Media.IS_ALARM, true)
        values.put(MediaStore.Audio.Media.IS_MUSIC, false)

        val uri = MediaStore.Audio.Media.getContentUriForPath(file.absolutePath)
        val newUri = app.context().contentResolver.insert(uri, values)

        try {
            RingtoneManager.setActualDefaultRingtoneUri(app.context(), RingtoneManager.TYPE_RINGTONE, newUri)

            //app.context().contentResolver.insert(MediaStore.Audio.Media.getContentUriForPath(file.absolutePath), values)

        } catch (t: Throwable) {
            log(t)
        }
    }

    companion object {
        fun fromDocument(document: QueryDocumentSnapshot): Rofl {
            val documentId = document.id
            val rofl = document.toObject(Rofl::class.java)
            rofl.id = documentId
            return rofl
        }
    }
}