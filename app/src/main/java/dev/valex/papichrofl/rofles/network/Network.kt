package dev.valex.papichrofl.rofles.network

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dev.valex.papichrofl.rofles.network.models.SetLikedResponse
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface Network {

    @GET("setLike")
    fun setLiked(@Query("roflId") roflId: String,
                 @Query("userId") userId: String,
                 @Query("liked") liked: Int): Call<SetLikedResponse>

    companion object {
        fun getHttpClient(): OkHttpClient {
            val timeout: Long = 30
            val httpClient = OkHttpClient.Builder().readTimeout(timeout, TimeUnit.SECONDS).writeTimeout(timeout, TimeUnit.SECONDS)
                    .connectTimeout(timeout, TimeUnit.SECONDS)
            httpClient.addInterceptor { chain ->
                val original = chain.request()
                val request = original.newBuilder()
                        .header("Accept", "application/json")
                        .method(original.method(), original.body())
                        .build()
                chain.proceed(request)
            }
            return httpClient.build()
        }

        fun getGson(): Gson {
            return GsonBuilder()
                    .setLenient()
                    .create()
        }
    }
}

class RetrofitCallback <T> (val onSuccess: (response: T?) -> Unit, val onError: (error: Throwable?) -> Unit = {}) : Callback<T> {
    override fun onResponse(call: Call<T>?, response: retrofit2.Response<T>?) {
        onSuccess.invoke(response?.body())
    }
    override fun onFailure(call: Call<T>?, t: Throwable?) = onError.invoke(t)
}