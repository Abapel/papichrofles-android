package dev.valex.papichrofl.rofles.interfaces

interface AdActivityLyfecycleListener {
    fun onPause()
    fun onResume()
    fun onDestroy()
}