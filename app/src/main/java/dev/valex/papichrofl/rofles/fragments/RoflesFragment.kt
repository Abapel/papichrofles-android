package dev.valex.papichrofl.rofles.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dev.valex.papichrofl.R
import dev.valex.papichrofl.databinding.FragmentRoflesBinding
import dev.valex.papichrofl.general.Papich.Companion.app
import dev.valex.papichrofl.general.base.BaseActivity
import dev.valex.papichrofl.general.base.BaseFragment

class RoflesFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = DataBindingUtil.inflate<FragmentRoflesBinding>(inflater, R.layout.fragment_rofles, container, false)
        binding.model = app.roflesViewModel()
        return binding.root
    }

    override fun onPause() {
        super.onPause()
        app.roflesViewModel().pause()
    }

    override fun onResume() {
        super.onResume()
        app.roflesViewModel().resume(activity as BaseActivity)
    }
}