package dev.valex.papichrofl.rofles.adapters

import android.databinding.BindingAdapter
import android.graphics.drawable.Animatable
import android.support.v4.widget.SwipeRefreshLayout
import android.widget.ImageView
import com.my.target.ads.MyTargetView
import dev.valex.papichrofl.general.utils.log
import dev.valex.papichrofl.rofles.interfaces.OnRoflActionListener
import dev.valex.papichrofl.rofles.models.Rofl
import dev.valex.papichrofl.rofles.views.RoflProgressBar
import dev.valex.papichrofl.rofles.views.RoflRecyclerView

@BindingAdapter("filterText")
fun bindItemClicked(recyclerView: RoflRecyclerView, filterText: String) {
    val adapter = recyclerView.adapter
    if (adapter is RoflesAdapter) {
        adapter.filterByName(filterText)
    }
}

@BindingAdapter("favourites")
fun bindFavourites(recyclerView: RoflRecyclerView, favourites: Boolean) {
    val adapter = recyclerView.adapter
    if (adapter is RoflesAdapter) {
        adapter.showFavouritesOnly(favourites)
    }
}

@BindingAdapter("addedRofles")
fun bindAddedRofles(recyclerView: RoflRecyclerView, rofles: List<Rofl>?) {
    if (rofles != null) {
        for (rofl in rofles) {
            recyclerView.getRoflesAdapter().addItem(rofl)
        }
    }
}

@BindingAdapter("updatedRofles")
fun bindUpdatedRofles(recyclerView: RoflRecyclerView, rofles: List<Rofl>?) {
    if (rofles != null) {
        for (rofl in rofles) {
            recyclerView.getRoflesAdapter().updateItem(rofl)
        }
    }
}

@BindingAdapter("removedRofles")
fun bindRemovedRofles(recyclerView: RoflRecyclerView, rofles: List<Rofl>?) {
    if (rofles != null) {
        for (rofl in rofles) {
            recyclerView.getRoflesAdapter().deleteItem(rofl)
        }
    }
}

@BindingAdapter("onRoflAction")
fun bindListener(recyclerView: RoflRecyclerView, listener: OnRoflActionListener) {
    recyclerView.getRoflesAdapter().onRoflActionListener = listener
}

@BindingAdapter("refreshing")
fun bindRefreshing(swipeRefreshLayout: SwipeRefreshLayout, refreshing: Boolean) {
    swipeRefreshLayout.isRefreshing = refreshing
}

@BindingAdapter("progressPercent")
fun bindProgress(roflProgressBar: RoflProgressBar, progressPercent: Int) {
    roflProgressBar.setProgressPercent(progressPercent)
}

@BindingAdapter("animating")
fun bindAnimating(imageView: ImageView, animating: Boolean) {
    val drawable = imageView.drawable
    if (drawable is Animatable) {
        if (animating) {
            drawable.start()
        }
        else {
            drawable.stop()
        }
    }
}

@BindingAdapter("searching")
fun bindSearching(imageView: ImageView, searching: Boolean) {
    val drawable = imageView.drawable
    if (drawable is Animatable) {
        drawable.start()
    }
}

@BindingAdapter("initAds")
fun bindErrorText(adView: MyTargetView, init: Boolean) {
    if (init) {
        adView.init(0)
        adView.listener = object : MyTargetView.MyTargetViewListener {
            override fun onLoad(p0: MyTargetView) {
                adView.start()
            }

            override fun onClick(p0: MyTargetView) {
                log("ad clicked")
            }

            override fun onNoAd(p0: String, p1: MyTargetView) {
                log("no ad")
            }
        }
        adView.load()
    }
}