package dev.valex.papichrofl.rofles.interfaces

import dev.valex.papichrofl.rofles.enums.RoflAction
import dev.valex.papichrofl.rofles.models.Rofl

interface OnRoflActionListener {
    fun onActionDone(action: RoflAction, rofl: Rofl)
}