package dev.valex.papichrofl.rofles.activities

import android.databinding.DataBindingUtil
import android.os.Bundle
import dev.valex.papichrofl.R
import dev.valex.papichrofl.databinding.ActivityRoflesBinding
import dev.valex.papichrofl.general.Papich.Companion.app
import dev.valex.papichrofl.general.base.BaseActivity

class RoflesActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityRoflesBinding>(this, R.layout.activity_rofles)
        binding.model = app.roflesViewModel()
    }

    override fun onPause() {
        super.onPause()
        app.roflesViewModel().pause()
    }

    override fun onResume() {
        super.onResume()
        app.roflesViewModel().resume(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        app.roflesViewModel().destroy()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        app.roflesViewModel().onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}