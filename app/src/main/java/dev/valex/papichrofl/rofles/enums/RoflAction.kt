package dev.valex.papichrofl.rofles.enums

enum class RoflAction {
    PLAY,
    LIKE,
    SHARE,
    DOWNLOAD,
    RINGTONE,
    NOTIFICAION
}