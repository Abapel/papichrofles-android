package dev.valex.papichrofl.rofles.viewmodels

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.databinding.BaseObservable
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.databinding.ObservableInt
import android.os.Build
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.firestore.*
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import dev.valex.papichrofl.R
import dev.valex.papichrofl.general.Papich
import dev.valex.papichrofl.general.Papich.Companion.analytics
import dev.valex.papichrofl.general.Papich.Companion.app
import dev.valex.papichrofl.general.Papich.Companion.config
import dev.valex.papichrofl.general.Papich.Companion.getUserId
import dev.valex.papichrofl.general.base.BaseActivity
import dev.valex.papichrofl.general.extentions.*
import dev.valex.papichrofl.general.utils.RoflMediaPlayer
import dev.valex.papichrofl.general.utils.download
import dev.valex.papichrofl.general.utils.log
import dev.valex.papichrofl.reportissue.activities.ReportIssueActivity
import dev.valex.papichrofl.rofles.enums.RoflAction
import dev.valex.papichrofl.rofles.interfaces.AdActivityLyfecycleListener
import dev.valex.papichrofl.rofles.models.Rofl
import dev.valex.papichrofl.rofles.models.RoflError
import dev.valex.papichrofl.rofles.network.RetrofitCallback
import dev.valex.papichrofl.rofles.network.models.SetLikedResponse
import dev.valex.papichrofl.settings.activities.SettingsActivity
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

import javax.inject.Inject
import kotlin.math.roundToInt

class RoflesViewModel @Inject constructor() : BaseObservable() {

    private val requestCodeWriteStorage = 1
    private val requestCodeChangeSettings = 2

    private val defaultCountdown = 5000L
    private val defaultDownloaded = 8000L
    private val configRefreshSeconds = 3600L //1 Hour
    private val adsKey = "adsEnabled"

    private val roflesReference = FirebaseFirestore.getInstance().collection("rofles")
    private var roflesListener: ListenerRegistration? = null
    private val mediaPlayer: RoflMediaPlayer = RoflMediaPlayer()
    private val handler: Handler = Handler()

    private var playerSubscription: Disposable? = null
    private var clickedRoflId: String? = null
    private var sortStrategy: Boolean = app.settings().isSortByLikes()

    var listBusy = ObservableBoolean(false)
    var favourites = ObservableBoolean(false)
    var searching: ObservableBoolean = ObservableBoolean(false)
    var searchText: ObservableField<String> = ObservableField("")
    var error: ObservableField<RoflError> = ObservableField()
    var fileDownloaded = ObservableBoolean(false)
    var sharingRofl: ObservableField<Rofl> = ObservableField()
    var emptyViewVisible: ObservableBoolean = ObservableBoolean(true)
    var servicesStatusCode: ObservableInt = ObservableInt(GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(app.context()))

    var askPermissionsActivity: BaseActivity? = null
    var onWriteStoragePermissionsGranted: () -> Unit = {}
    var onChangeSettingsPermissionsGranted: () -> Unit = {}

    var adsEnabled = ObservableBoolean(false)
    var adActivityLifecycleListener: AdActivityLyfecycleListener? = null

    init {
        refreshItems()
        cleanNotifications()
        getConfig()
    }

    fun getConfig() {
        config.fetch(configRefreshSeconds).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                config.activateFetched()
                showAds(config.isAdsEnabled())
            }
        }
    }

    fun showAds(show: Boolean) {
        adsEnabled.set(show)
    }

    fun setBusy(busy: Boolean) {
        listBusy.set(busy)
    }

    fun FirebaseRemoteConfig.isAdsEnabled(): Boolean = this.getBoolean(adsKey)

    fun cleanNotifications() {
        app.notificationManager().cancelAll()
    }

    fun DocumentSnapshot.containsData(): Boolean {
        return this.contains("name") && this.contains("storage") && this.contains("date")
    }

    fun refreshItems() {
        app.roflesAdapter().clear()
        resetPlayer()
        roflesListener?.remove()
        roflesListener = roflesReference
                .orderBy(if (app.settings().isSortByLikes()) "likedCount" else "date" , Query.Direction.DESCENDING)
                .addSnapshotListener { data, exc ->
                    if (data != null) {
                        for (change in data.documentChanges) {
                            val type = change.type
                            val document = change.document
                            try {
                                val rofl = Rofl.fromDocument(document)
                                when (type) {
                                    DocumentChange.Type.ADDED -> {
                                        app.roflesAdapter().addItem(rofl)
                                        emptyViewVisible.set(app.roflesAdapter().itemCount == 0)
                                    }
                                    DocumentChange.Type.MODIFIED -> {
                                        app.roflesAdapter().updateItem(rofl)
                                    }
                                    DocumentChange.Type.REMOVED -> {
                                        app.roflesAdapter().deleteItem(rofl)
                                        emptyViewVisible.set(app.roflesAdapter().itemCount == 0)
                                    }
                                }
                            }
                            catch (ignored: Throwable) {

                            }
                        }
                    }
                }
    }

    fun pause() {
        resetPlayer()
        adActivityLifecycleListener?.onPause()
        askPermissionsActivity?.unregisterReceiver(downloadReceiver)
    }

    fun resume(fromActivity: BaseActivity) {
        askPermissionsActivity = fromActivity
        askPermissionsActivity?.registerReceiver(downloadReceiver, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))
        adActivityLifecycleListener?.onResume()

        val newSortStrategy = app.settings().isSortByLikes()
        if (sortStrategy != newSortStrategy) {
            sortStrategy = newSortStrategy
            refreshItems()
        }
    }

    fun destroy() {
        adActivityLifecycleListener?.onDestroy()
    }

    fun setShowFileDownloaded(downloaded: Boolean, delay: Long = defaultDownloaded) {
        fileDownloaded.set(downloaded)
        handler.removeCallbacksAndMessages(null)
        handler.postDelayed({
            fileDownloaded.set(false)
        }, delay)
    }

    fun setErrorWithCountdown(_error: RoflError, countdown: Long = defaultCountdown) {
        error.set(_error)
        handler.removeCallbacksAndMessages(null)
        handler.postDelayed({
            error.set(null)
        }, countdown)
    }

    fun onSearchClicked() {
        searchText.set("")
        searching.set(!searching.get())
    }

    fun cancelSearch() {
        searchText.set("")
        searching.set(false)
    }

    fun showFavouritesOnly(value: Boolean) {
        cancelSearch()
        resetPlayer()
        favourites.set(value)
    }

    fun onSearchTextChanged(text: String) {
        searchText.set(text)
    }

    fun onSettingsClicked() {
        val intent = Intent(app.context(), SettingsActivity::class.java)
        askPermissionsActivity?.startActivity(intent)
    }

    fun onRoflActionDone(action: RoflAction, rofl: Rofl) {
        when (action) {
            RoflAction.PLAY -> onRoflPlayClick(rofl)
            RoflAction.LIKE -> onRoflLikeClick(rofl)
            RoflAction.SHARE -> onRoflShareClick(rofl)
            RoflAction.DOWNLOAD -> onRoflDownloadClick(rofl)
            RoflAction.RINGTONE -> onRoflRingtoneClick(rofl)
            RoflAction.NOTIFICAION -> onRoflNotificationClick(rofl)
        }
    }

    fun onRoflPlayClick(rofl: Rofl) {
        if (hasWriteStoragePermissions()) {
            onRoflPlayClickWithGrantedPermissions(rofl)
        }
        else {
            requestWriteStoragePermissions()
            onWriteStoragePermissionsGranted = {
                onRoflPlayClickWithGrantedPermissions(rofl)
            }
        }
    }

    fun onRoflPlayClickWithGrantedPermissions(rofl: Rofl) {
        clickedRoflId = rofl.id
        if (rofl.isFileExist()) {
            if (rofl.isPlayingNow()) {
                resetPlayer()
            }
            else {
                playRofl(rofl)
            }
        }
        else {
            resetPlayer()
            rofl.downloadWithTimeout(
                    OnSuccessListener { taskSnapshot ->
                        if (clickedRoflId == rofl.id) {
                            playRofl(rofl)
                        }
                    },
                    OnFailureListener { exception ->
                        setErrorWithCountdown(RoflError(exception.localizedMessage))
                    },
                    onTimeout = {
                        setErrorWithCountdown(RoflError(app.context().getString(R.string.timeout_error)))
                    })
            analytics.onRoflDownload(rofl)
        }
    }

    fun onRoflLikeClick(rofl: Rofl) {

        if (rofl.isLikedByUser()) {
            analytics.onRoflDislike(rofl)
        } else {
            analytics.onRoflLike(rofl)
        }

        val newValue = !rofl.isLikedByUser()
        val newValueParam = if (newValue) 1 else 0
        rofl.updatingLike.set(true)
        app.network().setLiked(rofl.id, getUserId(), newValueParam)
                .enqueue(RetrofitCallback<SetLikedResponse>({ response ->
                    log(response)
                    rofl.updatingLike.set(false)
                    if (response?.success == true) {
                        //rofl.setFakeLiked(newValue)
                    }
                }, { error ->
                    rofl.updatingLike.set(false)
                    setErrorWithCountdown(RoflError(app.context().getString(R.string.error_no_internet)))
                }))
    }

    fun onRoflShareClick(rofl: Rofl) {
        if (sharingRofl.get() == null) {
            sharingRofl.set(rofl)
            rofl.getDownloadUrlWithTimeout(
                    OnSuccessListener { uri ->
                        shareRoflWithUrl(rofl, uri.toString())
                        sharingRofl.set(null)
                    },
                    OnFailureListener { exception ->
                        sharingRofl.set(null)
                        setErrorWithCountdown(RoflError(app.context().getString(R.string.share_error)))
                    },
                    onTimeout = {
                        sharingRofl.set(null)
                        setErrorWithCountdown(RoflError(app.context().getString(R.string.timeout_error)))
                    })

            Papich.analytics.onRoflShare(rofl)
        }
    }

    fun onRoflDownloadClick(rofl: Rofl) {
        if (sharingRofl.get() == null) {
            sharingRofl.set(rofl)
            rofl.getDownloadUrlWithTimeout(
                    OnSuccessListener { uri ->
                        app.downloadManager().download(rofl.name, uri)
                        sharingRofl.set(null)
                    },
                    OnFailureListener { exception ->
                        sharingRofl.set(null)
                        setErrorWithCountdown(RoflError(app.context().getString(R.string.share_error)))
                    },
                    onTimeout = {
                        sharingRofl.set(null)
                        setErrorWithCountdown(RoflError(app.context().getString(R.string.timeout_error)))
                    })

            Papich.analytics.onRoflDownloadFile(rofl)
        }
    }

    val downloadReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            setShowFileDownloaded(true)
        }
    }

    fun onOpenDownloadsClick() {
        askPermissionsActivity?.startActivity( Intent(DownloadManager.ACTION_VIEW_DOWNLOADS))
    }

    fun onRoflRingtoneClick(rofl: Rofl) {
        if (hasChangeSettingsPermissions()) {
            rofl.setRingtone()
        }
        else {
            /*
            val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS)
            intent.data = Uri.parse("package:${app.context().packageName}")
            app.context().startActivity(intent)
            */

            requestChangeSettingsPermissions()
            onChangeSettingsPermissionsGranted = {
                rofl.setRingtone()
            }
        }
    }

    fun onRoflNotificationClick(rofl: Rofl) {
        //rofl.setRingtone()
    }

    fun shareRoflWithUrl(rofl: Rofl, url: String) {
        val intent = Intent(Intent.ACTION_SEND)
        val text = "${rofl.name}. ${app.context().getString(R.string.download)}: $url"
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_SUBJECT, rofl.name)
        intent.putExtra(Intent.EXTRA_TEXT, text)
        askPermissionsActivity?.startActivity(Intent.createChooser(intent, rofl.name))
    }

    fun onReportIssueClick() {
        val intent = Intent(app.context(), ReportIssueActivity::class.java)
        askPermissionsActivity?.startActivity(intent)
    }

    fun playRofl(rofl: Rofl) {
        resetPlayer()
        try {
            mediaPlayer.playRofl(rofl)
            mediaPlayer.setOnCompletionListener {
                rofl.playing.set(false)
                resetPlayer()
            }
            rofl.playing.set(true)
            playerSubscription = Observable.interval(50, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { value ->
                        val progressValue = 100.0 * mediaPlayer.currentPosition / mediaPlayer.duration
                        rofl.playProgressPercent.set(progressValue.roundToInt())
                    }
            analytics.onRoflPlay(rofl)
        }
        catch (exc: Exception) {
            setErrorWithCountdown(RoflError(app.context().getString(R.string.play_error)))
        }
    }

    fun resetPlayer() {
        playerSubscription?.dispose()
        if (mediaPlayer.isPlaying) {
            mediaPlayer.stop()
        }
        mediaPlayer.reset()
    }

    fun hasWriteStoragePermissions(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return ContextCompat.checkSelfPermission(app.context(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
        }
        return true
    }

    fun requestWriteStoragePermissions() {
        if (askPermissionsActivity != null) {
            ActivityCompat.requestPermissions(
                    askPermissionsActivity!!,
                    arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    requestCodeWriteStorage)
        }

    }

    fun hasChangeSettingsPermissions(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return ContextCompat.checkSelfPermission(app.context(), android.Manifest.permission.WRITE_SETTINGS) == PackageManager.PERMISSION_GRANTED
        }
        return true
    }

    fun requestChangeSettingsPermissions() {
        if (askPermissionsActivity != null) {
            ActivityCompat.requestPermissions(
                    askPermissionsActivity!!,
                    arrayOf(android.Manifest.permission.WRITE_SETTINGS),
                    requestCodeChangeSettings)
        }

    }

    fun IntArray.permissionsGranted(): Boolean {
        return isNotEmpty() && this[0] == PackageManager.PERMISSION_GRANTED
    }

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            requestCodeWriteStorage -> {
                if (grantResults.permissionsGranted()) {
                    onWriteStoragePermissionsGranted.invoke()
                }
            }
            requestCodeChangeSettings -> {
                if (grantResults.permissionsGranted()) {
                    onChangeSettingsPermissionsGranted.invoke()
                }
            }
        }
    }
}